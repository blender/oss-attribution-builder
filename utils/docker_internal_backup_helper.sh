#!/bin/bash

DATE=`date +%Y%m%d-%k:%M:%S`

if [[ ! -d /var/lib/postgresql/data/dumps/ ]]
then
	mkdir /var/lib/postgresql/data/dumps
fi

su postgres -c /usr/lib/postgresql/13/bin/pg_dump postgres > /var/lib/postgresql/data/dumps/pgsql-dump-$DATE.pgsql

