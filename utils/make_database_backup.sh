#!/bin/bash

OUR_CONTAINER=`docker ps -f "label=blender.oss-attribution-builder.container=database" --format "{{.ID}}"`
docker container exec $OUR_CONTAINER bash /utils/docker_internal_backup_helper.sh
